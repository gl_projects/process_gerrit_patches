#!/bin/bash

PWD=`pwd`
PLIST="patches.ps"
Change_Id=""
Patch_Desc=""
MissedChId=0

usage() {
	echo "use $0 N"
}

if [ $# -ne 1 ]; then
	usage
	exit 1
fi

re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
	usage
	exit 1
fi

Count=$1
for ((i=0;i < $Count;i++)); do
	Patch_Desc=`git log HEAD~$i --pretty=format:'%B' -1 | head -1`
	Change_Id=`git log HEAD~$i --pretty=format:'%B' -1 | grep Change-Id:`
	if [ "$Change_Id" == "" ]; then
		MissedChId=1
	fi
	Change_Id=${Change_Id#"Change-Id: "}
	echo $Change_Id - $Patch_Desc
	echo $Change_Id - $Patch_Desc >> $PLIST
done

if [ $MissedChId -ne 0 ]; then echo "---> WARNING: Some Change-Id are absent!" ; fi
echo "---> All done."
