#!/bin/bash

PWD=`pwd`
PLIST="patches.ps"
OUTFILE="out.txt"
JQ_UTIL="jq"
PATCHES=""

GERRIT_PORT="29419"
GERRIT_HOST="gerrit.globallogic.com.ua"

generate_patches_desc=0
do_review_verify=0
new_desc_view=0
wrong_param=0
cmd_id="commit"

# use __util_is_present file1 file2 ...
__util_is_present() {
	while [ "$1" ]
	do
		if [ "$(basename "${1}")" = "${1}" ]; then
			which ${1} >/dev/null
			if [ $? -eq 1 ]; then
				echo "---> Error: utility '${1}' not found"
				return 1
			fi
		else
			if [ ! -f ${1} ]; then
				echo "---> Error: file '${1}' does not exist"
				return 1
			fi
		fi
		shift
	done
	return 0
}

check_jq_utility() {
	if ! __util_is_present ${JQ_UTIL} ; then
		echo "---> Install '${JQ_UTIL}': $ sudo apt-get install ${JQ_UTIL}"
		exit
	fi
}

parse_script_parameters() {
	local usage_str="---> Use: $0 [generate_patches_desc] [do_review_verify] [new_desc_view] [use_changeid_instead_commit_id]"

	while [ "$1" ]
	do
		case "$1" in
			generate_patches_desc) generate_patches_desc=1;;
			do_review_verify) do_review_verify=1;;
			new_desc_view) new_desc_view=1;;
			use_changeid_instead_commit_id) cmd_id="change";;
			*) wrong_param=1;;
		esac
		shift
	done

	if [ ${wrong_param} -eq 1 ]; then
		echo "$usage_str"
	else
		echo "---> Input patch list file: '${PLIST}'"
	fi

	if [ "$cmd_id" = "change" ]; then
		echo "---> Use script 'get_patches_list.sh' to generate patches list"
	else
		echo "---> Use git to generate patches list (from branch, pushed to gerrit):"
		echo "---> $ git log --pretty=format:'%H -%d %s (%cr) <%an>' --abbrev-commit -N > ${PLIST}"
		echo "---> where N - patches count"
	fi

	if [ ${wrong_param} -eq 1 ]; then
		exit
	fi

	if [ ${generate_patches_desc} -eq 1 ]; then
		echo "---> Patches list will be written to the file '${OUTFILE}'"
	fi

	if [ ${do_review_verify} -eq 1 ]; then
		echo "---> Each patch WILL be reviewed and verified on gerrit (${GERRIT_HOST})"
	fi

	echo
}

parse_patch_list() {
	local PLIST_SET
	local i
	local ID
	local REM
	local DONE

	if [ ! -f $PWD/$PLIST ]; then
		echo "---> No PatchList file."
		return
	fi

	PLIST_SET=$PLIST

	echo "---> Patch file: " $PLIST_SET
	for i in $PLIST_SET; do
		DONE=false
		until $DONE; do
			read -r ID || DONE=true
			REM=`echo $ID | grep '#'`
			if [ ! -z $REM ]; then 
				continue
			fi
			ID=`echo $ID | awk '{print $1}'`
			PATCHES="$ID $PATCHES"
		done < $PWD/$i
	done
}

process_patches_list() {
	local PS
	local STR
	local SUBJECT
	local URL
	local cur_patch=1

	if [ ${generate_patches_desc} -eq 1 ]; then
		echo "---------------- Patches list ----------------" > ${OUTFILE}
	fi

	for PS in $PATCHES; do
		echo
		echo "---------------- $PS - Patch $cur_patch ----------------"

		STR=`ssh -p $GERRIT_PORT $GERRIT_HOST gerrit query --format=JSON $cmd_id:$PS`
		if [ $? -ne 0 ]; then
			echo "---> Gerrit access error. Host: '${GERRIT_HOST}', port: '${GERRIT_PORT}'. Please, try again. Script will be terminated."
			exit
		fi

		URL=`echo $STR | ${JQ_UTIL} '.url' | head -1 | sed 's/.\(.\+\).$/\1/'`
		SUBJECT=`echo $STR | ${JQ_UTIL} '.subject' | head -1 | sed 's/.\(.\+\).$/\1/' | sed 's/\\\"/\"/g'`

		if [ ${generate_patches_desc} -eq 1 ]; then
			if [ ${new_desc_view} -eq 1 ]; then
				echo "$SUBJECT" >> ${OUTFILE}
				echo "$URL" >> ${OUTFILE}
				echo "" >> ${OUTFILE}
			else
				echo "$URL - $SUBJECT" >> ${OUTFILE}
			fi
		fi

		echo "$URL - $SUBJECT"

		if [ ${do_review_verify} -eq 1 ]; then
			ssh -p $GERRIT_PORT $GERRIT_HOST gerrit review --verified +1 --code-review +1 $PS
		fi

		cur_patch=$(( $cur_patch + 1 ))
	done
}

check_jq_utility
parse_script_parameters $@
parse_patch_list
process_patches_list

echo "---> All done."
